"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if n % 20 == 1:
        print("Пожалуйста,", n, "яблоко")
    elif 2 <= n % 20 <= 4:
        print("Пожалуйста,", n, "яблока")
    elif 0 <= n <= 30:
        print("Пожалуйста,", n, "яблок")
    else:
        print("Столько нет")


if __name__ == "__main__":
    termcolor.cprint("Grocery assistant", 'green')
    main()
