# grocery-assistant

- [ ] Fix _apple.py._ Put the word “яблоко” in the correct form.
-
In order to verify the script, run the following commands:

	pip install -r requirements.txt (install required packages)
	python apple.py
	tox -r (run tests and code checkers)

Fix all issues found during validation.

- [ ] Use termcolor library to highlight caption “Grocery assistant”:

Install library locally:

    pip install termcolor
Add termcolor to _requirements.txt_.

Import library in the apple.py, e.g., _“from termcolor import colored”_

Highlight caption with any color or/and effect.
